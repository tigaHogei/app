Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  #Hauptsite
  root 'posts#index'
  #Example of regular route:
  # get 'products/:id' => 'catalog#view'
  #coustom  weg
  get 'about' => 'pages#about'

  #Example resource routes
  #resources :products
  resources :posts


end
