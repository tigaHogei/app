class PostsController < ApplicationController
  before_action :find_id, only: [:show,:update,:destroy,:edit]

  def index
    @Posts = Post.all
  end

  def show
  end

  def new
    @post = Post.new
  end

  def create
    #render plain: params[:post].inspect

    #sicherheit das nur itle und Inhalt übertragen werden können
    # ich habe ja ne private methode gschrien´ben für den anhängsel
    # @post = Post.create(params.require(:post).permit(:title,:body))
    @post = Post.create(post_params)

    if @post.save
      redirect_to  root_path
    else
      render :new
    end
  end

##    @post = Post.new(post_params)
##    @post.save
##    redirect_to @post

    def edit
    end

    def update
      if @post.update(post_params)
        redirect_to root_path
      else
        render :edit
      end
    end

    def destroy
      @post.destroy
      redirect_to root_path
    end



##  private
  def post_params
    params.require(:post).permit(:title, :body)
  end

  def find_id
    @post = Post.find(params[:id])
  end

end
